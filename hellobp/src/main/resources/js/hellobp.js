Confluence.Blueprint.setWizard('com.atlassian.confluence.plugins.myplugin:blueprint-item', function(wizard) {
	 wizard.on('submit.page1Id', function(e, state) {
	        var myName = state.pageData.myName;
	        alert('my name: ' + myName);
	        if (myName == 'abc') {
	            alert('That is not a real name!');
	            return false;
	        }
	    });
});